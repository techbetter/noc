# Define VM memory usage through environment variables
MEMORY = ENV.fetch("VAGRANT_MEMORY", "512")
#
$salt_ip_grain = <<SALT_IP_GRAIN
  if [[ -f /usr/bin/salt-call ]]; then
      salt-call --local --log-level=quiet --no-color grains.get ip4_interfaces:eth1 --output=json | python -c 'import sys,json; print json.load(sys.stdin)[\"local\"][0]' > /vagrant/.ip
  else
      echo 'Make sure you update /vagrant/.ip to your IP, it was not done automatically'
  fi
SALT_IP_GRAIN

$post_up_message = <<POST_UP
   ________________________________________________________________
  |                                                                |
  |   This is a Vagrant NOC local development VM for operations.   |
  |                                                                |
  |   You should now be able to launch minions from ./minions/     |
  |________________________________________________________________|

POST_UP

Vagrant.configure(2) do |config|
  config.vm.hostname = "noc.vagrant.alias.services"

  config.vm.box = "trusty-cloud"
  config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"
  config.ssh.forward_agent = true

  # ref: https://github.com/mitchellh/vagrant/issues/1673
  config.vm.provision "fix-no-tty", type: "shell" do |s|
      s.privileged = false
      s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  config.vm.network "private_network", type: "dhcp"

  if Vagrant.has_plugin?("vagrant-cachier")
    # Configure cached packages to be shared between instances of the same base box.
    # More info on http://fgrehm.viewdocs.io/vagrant-cachier/usage
    config.cache.scope = :box
    config.cache.enable :apt
  end

  config.vm.synced_folder "ops/configuration/formulas", "/srv/formulas", create: true
  config.vm.synced_folder "ops/configuration/pillars", "/srv/pillar", create: true
  config.vm.synced_folder "ops/configuration/states", "/srv/salt", create: true
  config.vm.synced_folder "ops/letsencrypt", "/etc/letsencrypt", create: true
  config.vm.synced_folder "webapps", "/srv/webapps", create: true

  # We would have had those files copied to /srv/webapps/.ssh.
  # Problem is that Vagrant won't let us because file provision won't let us,
  # so we'll keep a copy in vagran's homedir, but not webapps'.
  # Instead, refer to ops/configuration/states/level/vagrant/noc.sls
  config.vm.provision "file", source: ".ssh/id_rsa", destination: "/home/vagrant/.ssh/id_rsa", run: "always"
  config.vm.provision "file", source: ".ssh/id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub", run: "always"

  config.vm.provider :virtualbox do |v|
    v.name = config.vm.hostname
    v.customize ["modifyvm", :id, "--memory", MEMORY]
    v.customize ["modifyvm", :id, "--description", "Vagrant NOC workbench located in " + File.dirname(__FILE__) ]
    # ref: https://www.virtualbox.org/manual/ch08.html
    #      https://www.virtualbox.org/wiki/Guest_OSes
    v.customize ["modifyvm", :id, "--ostype", "Ubuntu_64"]
    v.customize ["modifyvm", :id, "--pae", "on"]
  end

  config.vm.provision :shell, path: "ops/provision/vagrant/updates.sh", run: "always"
  config.vm.provision :shell, path: "ops/provision/vagrant/bootstrap.sh"

  config.vm.provision :shell, inline: $salt_ip_grain, run: "always"

  config.vm.post_up_message = $post_up_message

end

# vi: set ft=ruby :
