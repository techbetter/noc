# Vagrant scripts

This folder contains shell scripts that we can invoke to build a VM using local
configuration **managed by Vagrant**.

Requirement this script bootstrapper should take care of is;

1. having salt-minion installed,
2. configured as "masterless",
3. "basesystem" formula cloned and accessible in /srv/formulas/basesystem
4. "basesystem" configured in salt stack's in `file_roots`

Since Vagrant can take care of applying 'highstate' and already mounts some
folders for us. We should make sure that basesystem is included and accessible
 in /srv/formulas/basesystem and mounted in file_roots array.

Rest should be handled by Salt Stack itself via the Vagrantfile.
