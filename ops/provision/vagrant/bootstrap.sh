#!/bin/bash

## Bootstrap a Vagrant host
##
## Should work on an Ubuntu 14.04 VM and
## common outcome is to have "level" grains and what's
## required to install Salt stack and apply states.

set -e

if [[ ! -d "/home/vagrant" || ! -d "/vagrant" ]]; then
  echo "This script is crafted only to run on a Vagrant VM and expects /vagrant AND /home/vagrant to exist."
  exit 3
fi

if [[ -d "/home/vagrant/.ssh" ]] && [[ -f "/home/vagrant/.ssh/authorized_keys" ]]; then
    echo 'Change authorized_keys to 600 to fix a bug in vagrant 1.8.5 that asks for a password on vagrant ssh.'
    chmod 600 /home/vagrant/.ssh/authorized_keys
fi

export INIT_LEVEL="vagrant"
export RUNAS=${INIT_LEVEL}

## Double quoting the URL might be strange, but if we do not, it won't execute.
curl -s -S -L "http://aliaswebservices.bitbucket.org/init.sh" | bash

. /vagrant/ops/provision/init.sh
