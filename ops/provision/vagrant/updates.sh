#!/bin/bash

set -e

## First time we do not find the file, just do it
if [[ ! -f /root/.vagrant_do_apt_update ]]; then
  apt-get update &&\
  apt-get -y upgrade &&\
  apt-get -y dist-upgrade &&\
  touch /root/.vagrant_do_apt_update
else
  ## Then, next bootups, conditionally update system
  if find /root/.vagrant_do_apt_update -mtime +4 | grep . > /dev/null 2>&1
  then
    echo 'We will be upgrading packages'
    apt-get update &&\
    apt-get -y upgrade &&\
    apt-get -y dist-upgrade &&\
    touch /root/.vagrant_do_apt_update
  else
    echo 'We will not be upgrading packages today, we did it recently already.'
  fi
fi
