#!/bin/bash

# Bootstrap a VM
#
# Should work on an Ubuntu 14.04 VM and
# common outcome is to have "level" grains and what's
# required to install Salt stack and apply states.
#

set -e

if [[ -z ${RUNAS} ]]; then
  echo 'Required variable RUNAS is not set'
  exit 1
fi

if [[ ! -x /usr/bin/salt-minion ]]; then
  echo 'Salt-Stack is not installed'
  exit 1
fi

if [[ "`hostname`" =~ "noc" ]]; then

  if [[ ! -x /usr/bin/salt-master ]]; then
    echo 'Salt-Stack Master is not installed!'
    exit 1
  fi

  if [[ ! -f /etc/salt/pki/master/minions/noc ]]; then
    echo 'Salt-Stack Master is not called "noc", and is not already accepted'
    if [[ -f /etc/salt/pki/master/minions_pre/noc ]]; then
      salt-key -y -a noc
    else
      echo 'And we could not auto-accept it'
      exit 1
    fi
  fi

  echo "This node will be a Salt Master"

  declare -A repos
  declare -A options

  salt-call --local ssh.set_known_host user=root hostname=bitbucket.org
  salt-call --local ssh.set_known_host user=root hostname=github.com

  ## Those should be taken from pillars, somewhere.
  ## There should be a way to get that data once, and import it elsewhere and
  ## ensure this gets to remain not duplicate data we already have in
  ## ../configuration/pillars/formulas #TODO
  repos["salt"]="git@bitbucket.org:AliasWebServices/salt-states.git"
  repos["pillar"]="git@bitbucket.org:AliasWebServices/pillars.git"
  repos["formulas/basesystem"]="https://github.com/renoirb/salt-basesystem"

  options["salt"]="--branch master --quiet"
  options["pillar"]="--branch master --quiet"
  options["formulas/basesystem"]="--branch master --quiet"

  for key in ${!repos[@]}; do
      if [ ! -d "/srv/${key}/.git" ]; then
        mkdir -p /srv/${key} && \
        chown ${RUNAS}:${RUNAS} /srv/${key} && \
        (salt-call --local git.clone /srv/${key} ${repos[${key}]} opts="${options[${key}]}" user="${RUNAS}" identity="/home/${RUNAS}/.ssh/id_rsa")
      else
        echo " * Repo in /srv/${key} already cloned. Did nothing."
      fi
  done

(cat <<- _EOF_
##
## File written here by ops/provision/init.sh.
##
file_roots:
  base:
    - /srv/salt
    # START Managed formula repositories -DO-NOT-EDIT-
    - /srv/formulas/basesystem
    # END Managed formula repositories --
_EOF_
) > /etc/salt/master.d/roots.conf
  test -L /etc/salt/minion.d/roots.conf || ln -s /etc/salt/master.d/roots.conf /etc/salt/minion.d/roots.conf
  echo "We will apply states (using state.highstate)"
  salt-call state.highstate --local --no-color -l debug
  service salt-master restart
fi

## ... rest will be managed by Salt!
