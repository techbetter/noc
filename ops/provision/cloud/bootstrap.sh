#!/bin/bash

## Bootstrap a VM on a Cloud provider
##
## Should work on an Ubuntu 14.04 VM and
## common outcome is to have "level" grains and what's
## required to install Salt stack and apply states.

set -e

if [[ -d "/home/vagrant" ]]; then
  echo "This script is crafted to run OUTSIDE a Vagrant VM"
  exit 4
fi

export RUNAS="ubuntu"

## Double quoting the URL might be strange, but if we do not, it won't execute.
curl -s -S -L "http://aliaswebservices.bitbucket.org/init.sh" | bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENT="$( dirname $DIR )"
REPO_ROOT="$( dirname $PARENT )"

if [[ -f /srv/salt/top.sls ]]; then
  echo "We will run highstate"
  salt-call state.highstate
fi
