# A VM on a cloud platform script

This folder contains shell scripts that we can invoke to build a VM using local
configuration.

Requirement this script bootstrapper should take care of is

1. having salt-minion installed,
2. configured as "masterless",
3. "basesystem" formula cloned and accessible in /srv/formulas/basesystem
4. "basesystem" configured in salt stack's in `file_roots`

End outcome of this script should be to have Salt stack make a 'highstate'.

From a new VM, as root, run:

    unzip thisrepo.zip
    INIT_LEVEL=staging bash thisrepo/ops/provision/cloud/bootstrap.sh

Take a look at the [README.md](../../../README.md) to read further.
