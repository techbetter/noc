# Your typical environment SSH keys goes here

Vagrant will import them inside the VM for you.

Call your SSH keys in this folder `id_rsa`, and `id_rsa.pub`
and that regardless of whether or not it's an RSA or an ECDSA key.
Script expects files with those names.

## If you don't have the pubkey at hand

You can extract the public key from a private key like this

    ssh-keygen -y -f .ssh/id_rsa > .ssh/id_rsa.pub

