# Network Operations Controller ("NOC")

This is the configuration repository for a builder node
whose purpose is to build and gather metrics from other web servers.

The following are instructions on how to have your own NOC.

Any procedure here should reflect the current setup method and be kept up to date.

[Required reading to understand the basic usage of Saltstack](https://docs.saltstack.com/en/latest/topics/tutorials/states_pt1.html).


## Use

### First steps

1. Clone this project
1. Run Make

      make deps

  Then you can follow instructions in **Locally with Vagrant** below.

  If you want to run this on a Cloud Provider, then also:

1. Go to your Cloud Provider management portal

1. Create a **Ubuntu 14.04 LTS** Server VM, use the following [Cloud-Init script](./ops/provision/cloud/noc-userdata.yml) or do manually the `runcmd`, `write_files` and `packages` blocks. See "Step 3: Configure Instance Details" *User Data** in AWS when launching a new Instance.

  * Ensure **/etc/salt/grains** file has level matching the level you want to create. This'll affect the hostname in **noc.$levelName.alias.services**.

1. Test you have Salt Stack intalled

    salt-call test.version --local
    local:
      2015.5.3

1. Create a folder to host pillars and states

```terminal
sudo mkdir -p /srv/{pillar,salt,formulas}
chown ubuntu:ubuntu /srv/{pillar,salt,formulas}
```

5. Copy the previously cloned repositories from your workstation to the new VM

```console
scp -avz -e ssh ops/configuration/states ubuntu@noc:/srv/salt
scp -avz -e ssh ops/configuration/pillars ubuntu@noc:/srv/pillar
```

6. Boostrap!

```console
salt-call state.apply --local
```

Then, continue with *Locally with Vagrant*. Because, we shouldn't work on states from a Cloud #ruleofthumb


### Locally with Vagrant

The `Vagrantfile` of this projects allows you to work locally on a Vagrant VM called `noc`.
All configuration code should build a main server, and allow you to deploy to secodary servers the *noc* manages.

To use, you need first to start the *noc* VM, then start the [minions](./minions/).

#### 1. Vagrant plugins

Assuming you already have [Vagrant installed][vagrant-site] and [VirtualBox][virtualbox-site]
installed on your host machine, you will also need the following plugins.

```console
vagrant plugin install vagrant-salt
vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-cachier
```

Note that only `vagrant-salt` plugin is mandatory. Others helps to speed things up.


#### 2. Add your local SSH keys

This project's purpose is to replicate deployment of code.
Most projects we deploy are most likely private projects and requires an SSH key.

In order to allow us to rebuild at any time the set of VMs,
we need to keep a local copy of the keys so we can consistently reuild it.

Make sure your have your public and private keys copied into the [.ssh/](./.ssh/) folder,
and make sure they are accessible as `.ssh/id_rsa` and `.ssh/id_rsa.pub`.

```console
mkdir .ssh
cp ~/.ssh/id_rsa .ssh/id_rsa
cp ~/.ssh/id_rsa.pub .ssh/id_rsa.pub
```

Notice that the files MUST be found exactly with this name because other scripts will
expect them and you'll get errors.

*Tip*, the `.ssh/` is in `.gitignore` so that you can keep a copy without
worries of commiting them by mistake.

#### 3. Launch the **noc** VM

By issuing the command;

    vagrant up

This should initialize the VM for its first run, but won't install everything
because we want things to start off quickly and want to use the same SSH keys
as we would use to deploy projects.

Once the VM is up, you can come into the VM and fetch all code repositories.

From your host, use Vagrant to get into the VM;

    vagrant ssh

Make sure the salt accepted itself;

    sudo salt-key

You should see salt in "Accepted Keys" list, otherwise;

    sudo salt-key -y -A

Notice that the state `level.vagrant` in [ops/configuration/states/level/vagrant/init.sls](./ops/configuration/states/level/vagrant/init.sls)
took care of copying your keys into `/srv/webapps/.ssh`.

Now we want to get the projects and deploy them.

The list of projects a cluster manage is described in [ops/configuration/pillars/projects/init.sls](./ops/configuration/pillars/projects/init.sls)

It's roughly a structured text, written in YAML, describing projects we'll manage on that cluster.

```yaml
projects:
  foo:
    origin: git@bitbucket.org:foo/foo.git
    branch: develop
    identity: /srv/webapps/.ssh/id_rsa
    server_name: .
    wordpress:
      config_filename: wordpress/env-local.php
    public_docroot: __www
mysql:
  projects:
    foo:
      database: foo
      username: foo
      password: some_secure_password
```

**foo**: unique project name
**origin**: Project git url.
**branch**: Project git branch to use.
**identity**: If we are using another ssh key (optional).
**server_name**: The server name to use in apache. If set to '.', it will use the project key and add *.vagrant.alias.services*.
**wordpress**: If we are using wordpress, copy a template env file to the specified directory. See [ops/configuration/states/projects/type/wordpress/files/env-local.php.jinja](./ops/configuration/states/projects/type/wordpress/files/env-local.php.jinja).
**public_docroot**: docroot for apache config file.

To see the list of configured projects, type this in the CLI:

    salt-call pillar.get projects

Which would give out

```yaml
local:
----------
foo:
    ----------
    branch:
        develop
    config_filename_dest:
        __wp_config/env-local.php
    identity:
        /srv/webapps/.ssh/id_rsa
    origin:
        git@bitbucket.org:foo/foo.git
    server_name:
        foo.com
    public_docroot:
        __www
```

We'll have the NOC node to clone the projects, but we first need a place to run the code.

Make web and db minions up and configured.

    cd minions
    vagrant up web0

Wait the first to finish before starting the second. Otherwise you may get apt conflicts due to the fact that three VMs will compete to use the same local cache because of [Vagrant Cachier plugin](http://fgrehm.viewdocs.io/vagrant-cachier/).

    vagrant up db0

This might take some time, wait for them to complete, maybe you'll have to restart salt-minion took

    vagrant ssh web0
    sudo service salt-minion restart

On the noc VM, as root:

    cd ../
    vagrant ssh

You should see which nodes are accepted from the new ones

    salt-key
    > Accepted Keys:
    > noc
    > Denied Keys:
    > Unaccepted Keys:
    > web0
    > db0

If you don't see them, you can go on and build projects in the mean time.

    salt-call -l debug state.apply projects.update

Which will clone the projects in `/srv/webapps/projects/` automatically for us. Assuming we have a project called "foo" like the example above, we'll have a git repository cloned in `/srv/webapps/projects/foo/`.

*HINT* While working on a NOC running with Vagrant, what's in `webapps/` gets mounted as `/srv/webapps/`. So we can thrash the NOC VM and use the same project clones across iterations.

Now assuming we can see nodes on `salt-key`, let's accept them and start them up

```console
salt-key -y -a web0
salt-ley -y -a db0
salt web0 state.apply
salt db0 state.apply
```

In a Vagrant workspace, we need a db0 VM. But in other environment we will use managed database cluster. More on this later.

Note that the apply on db0 takes care of creating database user and passwords so that web0 can access it.

Same thing goes for web0, all apache VirtualHosts and SSL certificates are installed automatically.

What we're doing is making the NOC node to have all code downloaded, and where we can run package management and build scripts so we can package them for deployment.

Let's build what the project needs:

```console
cd /srv/webapps/projects/foo
# Provided foo project has a package.json file, we install the dependencies
npm i
# We're using middleman with bower, let's make bower install its dependencies too
cd themes/themeName
bower install
cd ../../
git submodule update --init --recursive
gulp dev
```

Now we're ready to package and deploy.

Since NOC has the code ready to run, we can package and ship it:

    salt-call state.apply projects.package

Assuming we have a project called "foo" like the *project pillar* example cited earlier, we'll find the package in `/srv/salt/files/projects/foo-latest.tar.bz2`.

With this archive, we can now deploy the project to the Web servers.

To deploy, run:

    salt web0 state.apply projects.deploy

We're asking NOC to send to web0 VM the packages, unpack them.

The Apache VirtualHosts are handled at web0 "apply" (`salt web0 state.apply`) because the project list has `server_name` and the short name ("slug") to figure out what hostnames should be used.

If we changed project list recently and haven't updated server configuration, we can run "apply" at any time. That's how we want it to be anyway

    salt web0 state.apply

Now to see a deployed site, check the IP of the web0 node

    salt web0 grains.get ipv4
    web0:
        - 10.0.2.15
        - 127.0.0.1
        - 172.28.128.4

One of the given IPs should work.  In a Vagrant managed cluster, it might be the last IP that you can use ending by `.4`

The VirtualHosts files are generated according to the [projects pillar (i.e. what's in ops/configuration/pillars/projects/init.sls)](./ops/configuration/pillars/projects/init.sls). Assuming we have a project called "foo" like the *project pillar* example cited earlier, we'll find the VirtualHost file on web0 with lines similar to this:

    salt web0 cmd.run 'head /etc/apache2/projects.d/foo.conf'
    > ...
    > ServerName  foo.com
    > ServerAlias foo.vagrant.alias.services www.foo.vagrant.alias.services

Add to your local machine hosts file

    172.28.128.4 foo.vagrant.alias.services

Access it via a web browser

    http://foo.vagrant.alias.services/

Now let's configure a database server

Ensure we have databases and privileges available on database server:

    salt db0 state.apply stacks.mysql.databases

We can list the currently available databases on the DB node we created some minutes ago by doing

    salt db0 mysql.db_list
    db0:
        - information_schema
        - foo
        - mysql
        - performance_schema

Notice we already have a database called `foo`. That's because the `salt db0 state.apply` already created an empty database.

To see if the database has tables in it, you can run:

    salt db0 mysql.db_tables foo
    db0:

We could use empty database and make WordPress use it, but let's copy an existing database dump for the sake of documenting.

Imagine we have a MySQL Dump file at `ops/configuration/states/files/mysql/dumps/` called `foo-20150505.sql.gz`.

You can copy that database dump to the database minion and import the dump back to the server by doing the following in the CLI from the salt master:

```console
salt db0 cp.get_file salt://files/mysql/dumps/foo-20150505.sql.gz /srv/webapps/foo-20150505.sql.gz
salt db0 cmd.run 'gunzip foo-20150505.sql.gz' cwd=/srv/webapps/
salt db0 cmd.run 'mysql -u root foo < /srv/webapps/foo-20150505.sql'
```

To test if the import succeeded, you can run this command

    salt db0 mysql.db_tables foo
    db0:
        - wp_commentmeta
        - wp_comments
        - wp_links
        - wp_options
        - wp_postmeta
        - wp_posts
        - wp_term_relationships
        - wp_term_taxonomy
        - wp_termmeta
        - wp_terms
        - wp_usermeta
        - wp_users
        - wp_yarpp_related_cache

If you want to superseed for development all WordPress users for a site, you can do it using the following one-liner:

    salt db0 mysql.query foo 'UPDATE `wp_users` SET `user_pass` = "$P$BK/3Yafeg72Ziu2G72pMqtRPcZoBKj.";'

This will set to "admin" the password for any WordPress users that's using the "foo" database.

Voila.

  [vagrant-site]: https://www.vagrantup.com/
  [virtualbox-site]: https://www.virtualbox.org/

### WIP - SSL certificates

We can either re-use a pre-existing CA and Intermediate CA files in `/srv/salt/files/openssl/certificates/`, or let Salt create a new key pair for us.

*NOTE* What matters most about this is that we have only one Root CA package, for the company. That Root CA should be in the possession of the least number of people as possible because we'll have that Root CA to generate Intermediate CA and other child certificate types such as Web servers that'll require it and it's chain of trust. More will be written about the implications later on.

If we were to use a pre-existing Root CA and Intermediate Root CA, we would write them in the following locations

* Root CA:
  * `ops/configuration/states/files/openssl/certificates/certs/ca.crt`
  * `ops/configuration/states/files/openssl/certificates/private/ca.key`
* Intermediate Root CA:
  * `ops/configuration/states/files/openssl/certificates/intermediate/certs/intermediate.crt`
  * `ops/configuration/states/files/openssl/certificates/intermediate/private/intermediate.key`

*HINT* While working on a NOC running with Vagrant, what's in `ops/configuration/states/` gets mounted as `/srv/salt`.

The certificate and key that we really need are the *Intermediate Root CA*. The Root CA should only be used to create Intermediate. And as previously said, if we already have Intermediate Root CA files files in place, we'll have Salt create new ones for us.

If you haven't been provided an Intermediate Root CA package, run the following so that you can create your own Root and Intermediate Root sets:

    salt-call state.apply openssl.ca
    salt-call state.apply openssl.intermediate

If it's the first time you're running the configuration management on your workstation, you can create them yourself by running:

    salt-call state.apply openssl.certificates

We only need to run this command once at first installation, and if we change or add projects in [projects pillar (i.e. what's in ops/configuration/pillars/projects)](./ops/configuration/pillars/projects/init.sls).

The important detail to remember is that the Intermediate Root CA will sign our own sites certificates and manage them automatically.

Assuming we have a project called "foo" like the *project pillar* example cited earlier, we'll find the following:

* `ops/configuration/states/files/openssl/certificates/intermediate/private/foo.key` as the project's certificate key
* `ops/configuration/states/files/openssl/certificates/intermediate/private/foo.insecure.key` as the un-protected version of the certificate key
* `ops/configuration/states/files/openssl/certificates/intermediate/certs/foo.crt` as the *Intermediate Root CA* signed certificate file.
* `ops/configuration/states/files/openssl/certificates/intermediate/certs/foo.crt.txt` containing details about the cerficate such as *X509v3 Subject Alternative Name* allowing us to use more than one domain name for the same certificate file.
* `ops/configuration/states/files/openssl/certificates/intermediate/certs/foo.pem` which is a concatenation of `foo.crt` and `foo.insecure.key`, ready to be deployed to Web servers.
