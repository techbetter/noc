SHELL  := bash
PATH   := bin:${PATH}
PWD    := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))


.DEFAULT: vagrant


## References:
##  - https://www.gnu.org/software/make/manual/make.html
##  - http://makefiletutorial.com/


.PHONY: vagrant
vagrant: has_a_key
		$(info We have all dependencies, booting up.)
		vagrant up


.PHONY: deps
deps: has_a_key
		$(info Dependencies installed. Please make sure you have VirtualBox and Vagrant installed before moving on)


.ssh/id_rsa.pub: .ssh/id_rsa
		@ssh-keygen -y -f .ssh/id_rsa > .ssh/id_rsa.pub


.ssh/id_rsa:
		@ssh-keygen -q -N "" -f .ssh/id_rsa
		$(info Generated new private key)


.PHONY: has_a_key
has_a_key: .ssh/id_rsa.pub
		$(info Make sure you have this in your Bitbucket account preferences at SSH keys)
		$(info -------------------------------------------------------------------------)
		$(info $(shell cat .ssh/id_rsa.pub))
